class LineItem < ApplicationRecord
  belongs_to :book
  belongs_to :cart

  def total_price
    book.selling_price * quantity
  end
end
